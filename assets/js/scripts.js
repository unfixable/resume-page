/*
Theme Name: IAMX
Author: Ahmed Faruk
Author URL: farukahmed.com
*/

/*
    = Preloader
    = Animated scrolling / Scroll Up
    = Full Screen Slider
    = Sticky Menu
    = Back To Top
    = Countup
    = Progress Bar
    = More skill
    = Shuffle
    = Magnific Popup
    = Vidio auto play
    = Fit Vids
    = Google Map

*/

jQuery(function ($) {

    'use strict';

    /* ---------------------------------------------- /*
     * Preloader
    /* ---------------------------------------------- */

    $(window).ready(function () {
        $('#pre-status').fadeOut();
        $('#tt-preloader').delay(350).fadeOut('slow');
    });




    // -------------------------------------------------------------
    // Animated scrolling / Scroll Up
    // -------------------------------------------------------------

    (function () {
        $('a[href*=#]').bind("click", function (e) {
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top
            }, 1000);
            e.preventDefault();
        });
    }());



    // -------------------------------------------------------------
    // Full Screen Slider
    // -------------------------------------------------------------
    (function () {
        $(".tt-fullHeight").height($(window).height());

        $(window).resize(function () {
            $(".tt-fullHeight").height($(window).height());
        });

    }());


    // -------------------------------------------------------------
    // Sticky Menu
    // -------------------------------------------------------------

    (function () {
        $('.header').sticky({
            topSpacing: 0
        });

        $('body').scrollspy({
            target: '.navbar-custom',
            offset: 70
        })
    }());




    // -------------------------------------------------------------
    // Back To Top
    // -------------------------------------------------------------

    (function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scroll-up').fadeIn();
            } else {
                $('.scroll-up').fadeOut();
            }
        });
    }());


    // -------------------------------------------------------------
    // Countup
    // -------------------------------------------------------------
    $('.count-wrap').bind('inview', function (event, visible, visiblePartX, visiblePartY) {
        if (visible) {
            $(this).find('.timer').each(function () {
                var $this = $(this);
                $({
                    Counter: 0
                }).animate({
                    Counter: $this.text()
                }, {
                    duration: 2000,
                    easing: 'swing',
                    step: function () {
                        $this.text(Math.ceil(this.Counter));
                    }
                });
            });
            $(this).unbind('inview');
        }
    });


    // -------------------------------------------------------------
    // Progress Bar
    // -------------------------------------------------------------

    $('.skill-progress').bind('inview', function (event, visible, visiblePartX, visiblePartY) {
        if (visible) {
            $.each($('div.progress-bar'), function () {
                $(this).css('width', $(this).attr('aria-valuenow') + '%');
            });
            $(this).unbind('inview');
        }
    });

    // -------------------------------------------------------------
    // More skill
    // -------------------------------------------------------------
    $('.more-skill').bind('inview', function (event, visible, visiblePartX, visiblePartY) {
        if (visible) {
            $('.chart').easyPieChart({
                //your configuration goes here
                easing: 'easeOut',
                delay: 3000,
                barColor: '#68c3a3',
                trackColor: 'rgba(255,255,255,0.2)',
                scaleColor: false,
                lineWidth: 8,
                size: 140,
                animate: 2000,
                onStep: function (from, to, percent) {
                    this.el.children[0].innerHTML = Math.round(percent);
                }

            });
            $(this).unbind('inview');
        }
    });


    // -------------------------------------------------------------
    // Shuffle
    // -------------------------------------------------------------

    (function () {

        var $grid = $('#grid');

        $grid.shuffle({
            itemSelector: '.portfolio-item'
        });

        /* reshuffle when user clicks a filter item */
        $('#filter a').click(function (e) {
            e.preventDefault();

            // set active class
            $('#filter a').removeClass('active');
            $(this).addClass('active');

            // get group name from clicked item
            var groupName = $(this).attr('data-group');

            // reshuffle grid
            $grid.shuffle('shuffle', groupName);
        });


    }());


    // -------------------------------------------------------------
    // Magnific Popup
    // -------------------------------------------------------------

    (function () {
        $('.image-link').magnificPopup({

            gallery: {
                enabled: true
            },
            removalDelay: 300, // Delay in milliseconds before popup is removed
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below
            type: 'image'

        });

    }());

    $('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true 
      });

    $('.project_aoi').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=_alVflsVDH8'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });

    $('.project_mask').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=70J1k6QsgOI'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });


    $('.project_coco').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=4dB9sJeVfwA'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });


    $('.project_wearable').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=Q9Cm0-plwWs'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });


    $('.project_wearable2').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=a8mxbi-Zxeo'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });


    $('.project_seep').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=edPP3FN-sGw'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });


    $('.project_seep2').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=VdJv8v6Qaig'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });

    $('.project_spacetrucker').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=98YO_QjlKMU'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Some caption</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });


    $('.project_interpretari').magnificPopup({ 
        items: {
            src: 'https://www.youtube.com/watch?v=C7PX_trVCfE'
        },
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">'+
                '<div class="mfp-close"></div>'+
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '<div class="mfp-title">Interpretari</div>'+
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/', 
                    id: 'v=',
                    src: 'https://www.youtube.com/embed/%id%?rel=0&autoplay=1'
                }
            }
        }
        // other options
    });



    // -------------------------------------------------------------
    // Fit Vids
    // -------------------------------------------------------------
    (function () {
        $(".video-container").fitVids();
    }());








    // -------------------------------------------------------------
    // STELLAR FOR BACKGROUND SCROLLING
    // -------------------------------------------------------------

    $(window).load(function () {

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

        } else {
            $.stellar({
                horizontalScrolling: false,
                responsive: true
            });
        }

    });


    // -------------------------------------------------------------
    // WOW JS
    // -------------------------------------------------------------

    (function () {

        new WOW({

            mobile: false

        }).init();

    }());

    // -------------------------------------------------------------
    // Google Map
    // -------------------------------------------------------------

    (function () {

        var myLatlng = new google.maps.LatLng(48.1498652, 16.3108834);

        var styles = [{
            featureType: "landscape",
            stylers: [{
                color: '#f7f7f7'
            }]
        }, {
            featureType: "natural",
            stylers: [{
                hue: '#00ffe6'
            }]
        }, {
            featureType: "road",
            stylers: [{
                    hue: '#fff'
                },
                {
                    saturation: -70
                }
            ]
        }, {
            featureType: "building",
            elementType: "labels",
            stylers: [{
                hue: ''
            }]
        }, {
            featureType: "poi", //points of interest
            stylers: [{
                hue: ''
            }]
        }];

        var mapOptions = {
            zoom: 15,
            scrollwheel: false,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            styles: styles
        }
        var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            animation: google.maps.Animation.DROP,
            title: 'Hello World!'
        });

        var contentString = '' +
            '' +
            '';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });

    }());


});